## Setup Assignment

1. Setup the environment
    1. Rename .env.example to .env
    2. Run "composer install" and "composer dump_autoload"
    3. Run "yarn install" 
    4. Run "npm run dev"
    

2. Run API with postman
   - Login
        - url : /api/user/login
        - method : post
   - Form Data
       - email : "testuser@mail.com"
       - password : "12345678"
    
3. getSummaryByCountryAndCityOnDateRange
- url : /api/elasticsearch/getSummaryByCountryAndCityOnDateRange
- method : get
    - Headers
        - Authorization : Bearer<space>access_token
    - Params
        - startDate : provide start date format "yyyy-mm-dd"
        - endDate : provide end date format "yyyy-mm-dd"
        - size : integer value
    
4. getMostPopularSkuOnDateRange
- url : /api/elasticsearch/getMostPopularSkuOnDateRange
- method : get
    - Headers
        - Authorization : Bearer<space>access_token
    - Params
        - startDate : provide start date format "yyyy-mm-dd"
        - endDate : provide end date format "yyyy-mm-dd"


#### Missed items

* missed 3rd task on assignment  
* Unite test 
