<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Passport;

class LoginController extends Controller
{
    public function login(Request $request){
        $login = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string'
        ]);

//        return (Auth::attempt($login))?Auth::user()->createToken('authToken')->accessToken:"False";

        if(!Auth::attempt($login)){
            return response(['message' => 'invalid login credentials']);
        }

        $accessToken = Auth::user()->createToken('authToken')->accessToken;

        return response(['user'=>Auth::user(), 'access_token'=>$accessToken]);
    }
}
