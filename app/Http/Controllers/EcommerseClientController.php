<?php

namespace App\Http\Controllers;

use Elastica\Client as ElasticaClient;
use Elasticsearch\ClientBuilder;
use Illuminate\Http\Request;

class EcommerseClientController extends Controller
{
    protected $elasticsearch;
    protected $elastica;

    //setup client
    public function __construct()
    {
        $this->elasticsearch = ClientBuilder::create()->build();

        //Create an Elastica Client
        $elasticConfig = [
            'host' => env('Elastic_Host'),
            'port' => env('Elastic_Port'),
            'index' => env('Elastic_index')
        ];

        $this->elastica = new ElasticaClient($elasticConfig);
    }

//    public function elasticsearchTest()
//    {
//        $params = [
//            'index' => env('Elastic_index'),
//            'type' => '_doc',
//            'size' => '30',
//            'body' => [
//                'query' => [
//                    'range' => [
//                        'order_date' => [
//                            'gte' => '2021-04-29',
//                            'lte' => '2021-04-30'
//                        ]
//                    ]
//                ]
//            ]
//        ];
//
//        $response = $this->elasticsearch->search($params);
//        dump($response['hits']['hits']);
//    }

    public function getSummaryByCountryAndCityOnDateRange(Request $request)
    {
        $params = [
            'index' => env('Elastic_index'),
            'type' => '_doc',
//            'from' => '200',
            'size' => $request->size,
            'body' => [
                'query' => [
                    'range' => [
                        'order_date' => [
                            'gte' => $request->startDate,
                            'lte' => $request->endDate
                        ]
                    ]
                ],
                'aggs' => [
                    'country_agg' => [
                        'terms' => [
                            'field' => 'geoip.country_iso_code, geoip.city_name'
                        ]
                    ]
                ],
                'sort' => [
                    "geoip.country_iso_code" => 'asc',
                    "geoip.city_name" => 'asc'
                ]
            ]
        ];

        $response = $this->elasticsearch->search($params);
        return $response['hits']['hits'];
    }

    public function getMostPopularSkuOnDateRange(Request $request){
        $params = [
            'index' => env('Elastic_index'),
            'type' => '_doc',
            'size' => '0',
            'body' => [
                'query' => [
                    'range' => [
                        'order_date' => [
                            'gte' => $request->startDate,
                            'lte' => $request->endDate
                        ]
                    ]
                ],
                'aggs' => [
                    'sku_agg' => [
                        'terms' => [
                            "field"=> "sku"
                        ]
                    ]
                ],
            ]
        ];

        $response = $this->elasticsearch->search($params);
        return $response;
    }
}
